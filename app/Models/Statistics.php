<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'statistics';

    protected $fillable = ['ip','country','browser','os'];

    public function url()
    {
        return $this->belongsTo(URL::class);
    }


}
