<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class URL extends Model
{
    protected $table = 'urls';

    protected $fillable = ['source_url','short_url'];

    public function statistics()
    {
        return $this->hasMany(Statistics::class,'url_id');
    }

}
