<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Models\URL;
use Jenssegers\Agent\Agent;
use Stevebauman\Location\Facades\Location;


class URLRepository
{
    public function all()
    {
        return URL::cursor();
    }

    public function create($request)
    {
        $token = Util::shortUrl();

        URL::create([
            'source_url' => $request->source_url,
            'short_url' => $token
        ]);

        return true;
    }

    public function destination($url)
    {
        $destination = URL::where('short_url', $url);

        if ($destination->exists()) {
            $this->mapInfo($destination);
        }

        return Util::destination($destination);
    }

    public function statistics($url)
    {
        return $url->load('statistics')->toArray();
    }

    public function mapInfo($destination)
    {
        $location = Location::get();
        $agent = new Agent();

        $destination->first()->statistics()->create([
            'ip' => $location->ip,
            'country' => $location->countryName,
            'browser' => $agent->browser(),
            'os' => $agent->platform(),
        ]);

        return true;
    }


}
