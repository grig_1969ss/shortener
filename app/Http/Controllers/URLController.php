<?php

namespace App\Http\Controllers;

use App\Http\Requests\URL\UrlCreateRequest;
use App\Models\URL;
use App\Repositories\URLRepository;
use Illuminate\Http\Request;

class URLController extends Controller
{
    protected $URLRepository;

    public function __construct(URLRepository $URLRepository)
    {
        $this->URLRepository = $URLRepository;
    }

    public function index()
    {
        $urls = $this->URLRepository->all();

        return view('shortener.index', compact('urls'));
    }

    public function urlSharing($url)
    {
        $destination = $this->URLRepository->destination($url);

        return redirect($destination);
    }

    public function urlStatistics(URL $url)
    {
        $url = $this->URLRepository->statistics($url);

        return view('shortener.statistics', compact('url'));
    }

    public function store(UrlCreateRequest $request)
    {
        $this->URLRepository->create($request);

        return back();
    }
}
