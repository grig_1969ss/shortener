<?php

namespace App\Helpers;

use App\Models\URL;

class Util
{
    public static function shortUrl()
    {
        $number = mt_rand(0, PHP_INT_MAX);
        $shortUrl = strtr(rtrim(base64_encode(pack('i', $number)), '='), '+/', '-_');

        //we could use last id in table, but each next token is very similar to previous one

        $already = URL::where('short_url', $shortUrl);
        if (!$already->exists()) {
            return $shortUrl;
        }

        // for security reasons you can stop this loop after for example 100 iterations or send an email to admin
        self::shortUrl();
    }

    public static function destination($destination)
    {
        return $destination->exists() ? $destination->first()->source_url : '/';
    }
}
