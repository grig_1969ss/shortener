<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/','shorten');

Route::get('shorten','URLController@index');

Route::get('url-statistics/{url}','URLController@urlStatistics')->name('url.statistics');

Route::post('shorten', 'URLController@store')->name('shorten.create');

Route::get('{url}','URLController@urlSharing')->name('url.sharing');



