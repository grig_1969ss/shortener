@extends('layouts.main')

@section('container')

    <div class="container">
     <h1>Statistics for URL</h1>

        <div class="row">
            <a href="{{url('/shorten')}}" class="btn btn-success pull-right" >Back</a>
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Country</th>
                <th scope="col">Browser</th>
                <th scope="col">OS</th>
                <th scope="col">Ip</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($url['statistics'] as $statistic)
                <tr>
                    <td>{{$statistic['country']}}</td>
                    <td>{{$statistic['browser']}}</td>
                    <td>{{$statistic['os']}}</td>
                    <td>{{$statistic['ip']}}</td>
                    <td>{{$statistic['updated_at']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>


    </div>
@endsection
