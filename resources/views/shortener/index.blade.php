@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="text-center">
            <h1>URL Shortener</h1>
            <p class="lead">Find all URLs list here</p>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <button class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">Add Url</button>
        </div>

        @if($urls->count())
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Source URL</th>
                    <th scope="col">Sharing URL</th>
                    <th scope="col">Statistics</th>
                </tr>
                </thead>
                <tbody>
                @foreach($urls as $url)
                    <tr>
                        <td>{{$url->source_url}}</td>
                        <td><a href="{{route('url.sharing', $url->short_url)}}">{{route('url.sharing', $url->short_url)}}</a></td>
                        <td><a href="{{route('url.statistics', $url->id)}}">{{$url->short_url}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h4 class="text-center">Empty data</h4>
        @endif

    </div>
    @include('includes.url_modal')
@endsection
